package org.opencv.samples.tutorial1;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.HOGDescriptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;



public class Tutorial1Activity extends Activity implements CvCameraViewListener2 {
    private static final String TAG = "OCVSample::Activity";

    private CameraBridgeViewBase mOpenCvCameraView;
    private boolean              mIsJavaCamera = true;
    private MenuItem             mItemSwitchCamera = null;
    //HOG detector
    private HOGDescriptor hog;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    testing();
                    trainingDetector();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public Tutorial1Activity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.tutorial1_surface_view);

        if (mIsJavaCamera)
            mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.tutorial1_activity_java_surface_view);
        else
            mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.tutorial1_activity_native_surface_view);
        mOpenCvCameraView.setMaxFrameSize(480, 320); // Use a smaller captured frame
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);

        mOpenCvCameraView.setCvCameraViewListener(this);

    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback);
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "called onCreateOptionsMenu");
        mItemSwitchCamera = menu.add("Toggle Native/Java camera");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String toastMesage = new String();
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);

        if (item == mItemSwitchCamera) {
            mOpenCvCameraView.setVisibility(SurfaceView.GONE);
            mIsJavaCamera = !mIsJavaCamera;

            if (mIsJavaCamera) {
                mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.tutorial1_activity_java_surface_view);
                toastMesage = "Java Camera";
            } else {
                mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.tutorial1_activity_native_surface_view);
                toastMesage = "Native Camera";
            }
            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
            mOpenCvCameraView.setCvCameraViewListener(this);
            mOpenCvCameraView.enableView();
            Toast toast = Toast.makeText(this, toastMesage, Toast.LENGTH_LONG);
            toast.show();
        }

        return true;
    }

    public void onCameraViewStarted(int width, int height) {
    }

    public void onCameraViewStopped() {
    }

    //Process each frame
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        Mat src = inputFrame.rgba();

        return detect(src, 1);
    }

    //Banner, may be use for display FPS
    public void testing() {
        // make a mat and draw something
        Mat m = Mat.zeros(100,400, CvType.CV_8UC3);
        Core.putText(m, "Testing Mode", new Point(30,80), Core.FONT_HERSHEY_SCRIPT_SIMPLEX, 2.2, new Scalar(200,200,0),2);

        // convert to bitmap:
        Bitmap bm = Bitmap.createBitmap(m.cols(), m.rows(),Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(m, bm);

        // find the imageview and draw it!
        ImageView iv = (ImageView) findViewById(R.id.imageView1);
        iv.setImageBitmap(bm);
    }

    //training the HOG detector with pre - trained txt file
    public void trainingDetector(){
        //Set up for hog detector
        Log.i(TAG,"Start setting HOGDescriptor");
        hog = new HOGDescriptor(new Size(new Point(64, 64)), new Size(new Point(16, 16)), new Size(new Point(8, 8)), new Size(new Point(8, 8)), 9);

        InputStream is = getResources().openRawResource(+ R.raw.hogdetector);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        String temp = null;

        try {
            temp = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i(TAG,"read data success.");

        String[] detector_src = null;
        if(temp!=null)
            detector_src = temp.split(" ");
        else
            Log.i(TAG,"Null content");

        List<Float> tempList = new ArrayList<Float>();
        MatOfFloat detector = new MatOfFloat();

        for(String s: detector_src) {
            tempList.add(Float.parseFloat(s));
            // Log.i("data: ",s);
        }

        detector.fromList(tempList);

        hog.setSVMDetector(detector);

        Log.i(TAG, "Set SVM Detector successful!");
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //find largest object and return the bottom point
    public Point findLargestPoint(List<Rect> list, double scale){
        Rect temp = null;

        for(Rect rect : list)
        {
            if(temp == null || rect.area() > temp.area())
                temp = rect;
        }

        Point largest = new Point((temp.x + temp.width / 2) * scale, (temp.y + temp.height) * scale);

        return largest;
    }

    //HOG detection, "scale" use to scale down the input Mat
    public Mat detect(Mat src,double scale){
        if(src != null){
            Mat greyImg = new Mat();
            Imgproc.cvtColor(src, greyImg, Imgproc.COLOR_RGB2GRAY);
            Mat resizeImg = new Mat((int) Math.round(greyImg.rows() / scale), (int) Math.round(greyImg.cols() / scale), CvType.CV_8UC1);
            Imgproc.resize(greyImg, resizeImg, resizeImg.size(), 0, 0, Imgproc.INTER_LINEAR);
            Imgproc.equalizeHist(resizeImg, resizeImg);

            double t = Core.getTickCount();

            MatOfRect found = new MatOfRect();
            MatOfDouble foundWeights = new MatOfDouble();

            Log.d("Detect", "MultiScale detect");
            hog.detectMultiScale(resizeImg, found, foundWeights, 0.0, new Size(8, 8), new Size(0, 0), 1.2, 1, false);
            Log.d("Detect Num", "Detected Rect Num " + found.size());
            t = Core.getTickCount() - t;
            Log.d("detection time", "detection time = " + t / (Core.getTickFrequency() * 1e-3) + " ms");

            if (found.rows() > 0) {
                Point rectPoint1 = new Point();
                Point rectPoint2 = new Point();
                Point cirPoint = new Point();

                List<Rect> rectList = found.toList();
                int i = 0;
                for (Rect rect : rectList) {
                    rectPoint1.x = rect.x * scale;
                    rectPoint1.y = rect.y * scale;
                    rectPoint2.x = (rect.x + rect.width) * scale;
                    rectPoint2.y = (rect.y + rect.height) * scale;
                    cirPoint.x = (rect.x + rect.width / 2) * scale;
                    cirPoint.y = (rect.y + rect.height) * scale;

                    // Draw rectangle and red dot around fond object
                    Core.rectangle(src, rectPoint1, rectPoint2, new Scalar(255, 255, 0), 3);
                   Core.circle(src, cirPoint, 4, new Scalar(255, 0, 0), -1, 8, 0);
                }
                //Draw largest rectangle car with blue dot
                Core.circle(src, findLargestPoint(rectList,scale), 4, new Scalar(0,0,255), -1, 8, 0);
            }
            //Force jni library to reallocated the memory
            greyImg.release();
            found.release();
            resizeImg.release();
            foundWeights.release();
        }


        return src;
    }

}
